<?php

// put the all records in array
$sandwich = array(
		array('name' => 'The Sissy', 'price' => 4.99,
				'desc' => 'Just veggies; no meat on a whole wheat bun',
				'calories' => 450),
		array('name' => 'The Corleone', 'price' => 5.50,
				'desc' => 'Ham and salami with lettuce tomato and onions',
				'calories' => 980),
		array('name' => 'The Mediterranean', 'price' => 6.99,
				'desc' => 'Hummus, turkey, tomatoes, and olives on whole wehat',
				'calories' => 500),
		array('name' => 'The Greasy Pizza', 'price' => 8.00,
				'desc' => 'Meatballs, sauce, pepperoni, and cheese on a white bun',
				'calories' => 1500),
		array('name' => 'The Plain and Simple', 'price' => 5.50,
				'desc' => 'Turkey, lettuce, tomato, and mayo on a wheat bun',
				'calories' => 650),
		array('name' => 'The Porker', 'price' => 7.99,
				'desc' => 'Pulled pork, bbq sauce, and cream cheese on a toasted wheat bun',
				'calories' => 1350),
		array('name' => 'The Sub of Insanity', 'price' => 9.99,
				'desc' => 'The works; all of our toppings on one sandwich',
				'calories' => 2200));
// collect all price in separate array for price shorting
foreach ($sandwich as $key => $value) {
	$priceArr[$key] = $value['price'];
}
//short the array for pricing and maintain the index of sorted array
arsort($priceArr, SORT_NUMERIC);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<title>Sandwich Shop</title>
</head>
<body>
		<div style="font-weight: bold;margin-bottom:20px">Sandwich Shop</div>
		<table>
		<?php
foreach ($priceArr as $key => $value) {
		?>
			<tr>
				<td>Name:
				</td>
				<td>
					<?php echo $sandwich[$key]['name'] ?>
				</td>
			</tr>
			<tr>
				<td>Price:
				</td>
				<td>
					<?php 
					// string function is used here
					echo sprintf("%.2f", $sandwich[$key]['price']);

					?>
				</td>
			</tr>
			<tr>
				<td>Description:
				</td>
				<td>
					<?php echo $sandwich[$key]['desc'] ?>
				</td>
			</tr>
			<tr>
				<td>Calories:
				</td>
				<td>
					<?php echo $sandwich[$key]['calories'] ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr>
				</td>
				
			</tr>
		<?php
}
		?>
		</table>
</body>
</html>