<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1> Sandwich menu </h1>
        <?php
        // put your code here
        
        require('config.php');
        require('SandwichManager.class.php');
        $objSandwichManager = new SandwichManager($db_host,$db_user,  $db_name, $db_pass);
        $sandwiches = $objSandwichManager->Sandwich();
        print "<table>";
        foreach ($sandwiches as $sandwich) {
          print "<tr>" .
                "<td>" . $sandwich['Name']. "</td>" .
                "<td>" . $sandwich['Descr']. "</td>" .
                "<td>" . $sandwich['Price']. "</td>" .
                "<td>" . $sandwich['Calories']. "</td>" .
                "</tr>";
        }
        print "</table>";
        ?>
    </body>
</html>
