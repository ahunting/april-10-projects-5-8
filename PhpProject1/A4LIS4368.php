<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
	dir="ltr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title>Sandwich Shop</title>
</head>
<link rel="stylesheet" type="text/css" href="/style.css" />
<body>
	<div style="font-weight: bold; margin-bottom: 20px">Welcome to Whole Food Sandwich Shop
	</div>
	<div style="float:right">
		<a href="orderpage.php">Order your choice</a>
	</div>
	<div style="clear:both">
		&nbsp;
	</div>
	<table>
                                        <tr>
			<td>Name:</td>
			<td>
               The Sissy            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $4.99                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Just veggies; no meat on a whole wheat bun            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               450            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Corleone            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $5.50                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Ham and salami with lettuce tomato and onions            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               980            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Mediterranean            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $6.99                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Hummus, turkey, tomatoes, and olives on whole wehat            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               500            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Greasy Pizza            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $8.00                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Meatballs, sauce, pepperoni, and cheese on a white bun            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               1500            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Plain and Simple            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $5.50                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Turkey, lettuce, tomato, and mayo on a wheat bun            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               650            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Porker            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $7.99                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               Pulled pork, bbq sauce, and cream cheese on a toasted wheat bun            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               1350            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                                        <tr>
			<td>Name:</td>
			<td>
               The Sub of Insanity            </td>
		</tr>
		<tr>
			<td>Price:</td>
			<td>
               $9.99                                </td>
		</tr>
		<tr>
			<td>Description:</td>
			<td>
               The works; all of our toppings on one sandwich            </td>
		</tr>
		<tr>
			<td>Calories:</td>
			<td>
               2200            </td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>

		</tr>
                     </table>
</body>
</html>