<?php
class Sandwich_model extends CI_Model {
//Most immportant file.

	public function __construct()
	{
		$this->load->database();
	}
 
  public function get_sandwich()
  {
 
  //this is a method that retrieves a list of sandwiches and
  $query = $this->db->get('sandwich');
  	//this part returns that list as an array 
	return $query->result_array();
  }
  
 //A method that accepts parameters for your sub and saves them as a row in the Orders table (same way as Assignment 4).
 public function add_order()
  {
    /*
    create table orders (
    id int not null auto_increment,
    total decimal(10,2),
    address varchar(100),
    pay_type char(5),
    card_num int(20),
    month int(2),
    year int(4),
    cvv int(3),
    newsletter boolean,
    primary key(id)
    );
    */

    // retrieve the form
    $sandwiches  = $this->input->post('sandwiches');
    $quantities  = $this->input->post('quantities');
    $prices      = $this->input->post('prices');
    $address     = $this->input->post('address');
    $pay_type    = $this->input->post('pay_type');
    $card_num    = $this->input->post('card_num');
    $month       = $this->input->post('month');
    $year        = $this->input->post('year');
    $cvv         = $this->input->post('cvv');
    $newsletter  = $this->input->post('newsletter');

    // calculate the total pay
    $total=0.0;
    foreach ($quantities as $id => $quantity) {
      if (!empty($quantity)) $total = $total + $prices[$id]*$quantity;
    }

    // add 7.5% tax
    $total = $total + $total * .075;

    // add 1.5% processing fee if pay by credit card
    if ($pay_type=='credit') {
      $total *= 1.015;
    }

    if ($pay_type=='credit') {
	    $data=array(
        'total'      => $total,
        'address'    => $address,
        'pay_type'   => $pay_type,
        'card_num'   => $card_num,
        'month'      => $month,
        'year'       => $year,
        'cvv'        => $cvv,
        'newsletter' => $newsletter
	    );
    }else{
	    $data=array(
        'total'      => $total,
        'address'    => $address,
        'pay_type'   => $pay_type,
        'newsletter' => $newsletter
	    );
    }

	  $this->db->insert('orders', $data);
    
    return $total;
	}
	
//I've added two methods.
  public function get_sandwich_by_id($id)
  {
 
	//this is a method that retrieves sandwiches by its id
	$query = $this->db->get_where('sandwich', array('id'=>$id));

	$data = $query->result_array();
	if (empty($data)) {
		return FALSE;
	}else{
		return $data;
	}
  }
}

