<?php

class Sandwich extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('sandwich_model');
	}

	public function homepage()
	{
		$data['sandwich'] = $this->sandwich_model->get_sandwich();

		$this->load->view('homepage', $data);
	}
	
  public function orderpage()
  {
		$data['sandwich'] = $this->sandwich_model->get_sandwich();
//This satisfies the form validation part of the assignment.
//You must use the form validation and form helper libraries for your order form.
  	
	$this->load->helper(array('form','url'));
  	$this->load->library('form_validation');

    $this->form_validation->set_rules('quantities', 'Quantities', 'callback_quantities_check');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('pay_type', 'Pay type', 'required');
    if ($this->input->post('pay_type') == 'credit') {
      $this->form_validation->set_rules('card_num', 'Card number', 'required|integer');
      $this->form_validation->set_rules('month', 'Expired month', 'required|integer|max_length(2)');
      $this->form_validation->set_rules('year', 'Expired year', 'required|integer|max_length(4)');
      $this->form_validation->set_rules('cvv', 'CVV', 'required|integer|max_length(3)');
    }
    $this->form_validation->set_rules('newsletter', 'newsletter', '');
	
  	if ($this->form_validation->run() === FALSE)
  	{
      $data['quantities']=$this->input->post('quantities');
  		$this->load->view('orderpage', $data);
  	}
  	else
  	{
      $data['sandwiches']=$this->input->post('sandwiches');
      $data['prices']=$this->input->post('prices');
      $data['quantities']=$this->input->post('quantities');
      $data['total'] = $this->sandwich_model->add_order();
  		$this->load->view('ordersuccess', $data);
	  }
  }

  public function quantities_check($qs) {
    if (empty($qs)) {
      $this->form_validation->set_message('quantities_check', 'The %s field is required');
      return FALSE;
    }else{
      $s=0;
      foreach ($qs as $q) {
        if (!empty($q)) $s += $q;
      }
      if ($s==0) {
        $this->form_validation->set_message('quantities_check', 'The %s field is required');
        return FALSE;
      }else{
        return TRUE;
      }
    }
  }
}

