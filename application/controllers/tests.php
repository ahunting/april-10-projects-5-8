<?php

class Tests extends CI_Controller {
//This is the unit test controller that tests whether it passes
//5.  Use the Unit Testing Class to generate a report about whether or not the tests succeeded or failed.

	public function __construct()
	{
		parent::__construct();
		$this->load->model('sandwich_model');
		$this->load->library('unit_test');
	}

	public function index()
	{
	//Tests get_sandwich_by_id(1);
	
		$data = $this->sandwich_model->get_sandwich_by_id(1);
		$this->unit->run($data, 'is_array');

		$data = $this->sandwich_model->get_sandwich_by_id(-1);
		$this->unit->run($data, 'is_false');

		echo $this->unit->report();
// the following report showes the results of the unit test of the method get_sandwich_by_id		
/*
Test Name	Undefined Test Name
Test Datatype	Array
Expected Datatype	Array
Result	Passed
File Name	C:\wamp\www\Assignment7\application\controllers\tests.php
Line Number	17
Notes	
Test Name	Undefined Test Name
Test Datatype	Boolean
Expected Datatype	bool
Result	Passed
File Name	C:\wamp\www\Assignment7\application\controllers\tests.php
Line Number	20
Notes
*/
	}
	
}
