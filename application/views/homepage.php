<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
	dir="ltr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title>Sandwich Shop</title>
</head>
<link rel="stylesheet" type="text/css" href="/style.css" />
<body>
	<div style="font-weight: bold; margin-bottom: 20px">Welcome To Whole Foods
		Sandwich Shop
	</div>
	<div style="float:right">
		<a href="orderpage">Order your choice</a>
	</div>
	<div style="clear:both">
		&nbsp;
	</div>
	<table>
<?php 
//Home page that lists the sub sandwiches, the same way that your prior assignments have 
//(see CodeIgniter documentation for how to set a specific controller and method as the front page)
	foreach ($sandwich as $sandwich_item) {
    print "<tr>";
    print "<td>Name:</td>";
    print "<td>" . $sandwich_item['Name'] . "</td>";
    print "</tr>";

    print "<tr>";
    print "<td>Price:</td>";
    print "<td>" . $sandwich_item['Price'] . "</td>";
    print "</tr>";
		
    print "<tr>";
    print "<td>Description:</td>";
    print "<td>" . $sandwich_item['Descr'] . "</td>";
    print "</tr>";
		
    print "<tr>";
    print "<td>Calories:</td>";
    print "<td>" . $sandwich_item['Calories'] . "</td>";
    print "</tr>";
		
    print "<tr>";
    print "<td colspan=\"2\"><hr></td>";
    print "</tr>";
	}
?>
    </table>
</body>
</html>
