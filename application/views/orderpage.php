<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
      dir="ltr">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <title>Sandwich Shop</title>
  </head>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <body>
    <div style="font-weight: bold; margin-bottom: 20px">Welcome To Whole Foods Sandwich Shop
    </div>
    <div style="clear:both">
      &nbsp;
    </div>
<?php echo validation_errors(); ?>

<?php echo form_open('index.php/sandwich/orderpage') ?>

	<table>
<?php 
//A page that displays and processes your sub order form, with the same requirements as Assignment 4. 
// Use the Form Validation library and Form Helper to accomplish validation, displaying error messages, and refilling form values.
	foreach ($sandwich as $id => $sandwich_item) {
    print "<tr>\n";
    print "<td>Name:</td>\n";
    print "<td>" . $sandwich_item['Name'] . "<input type='hidden' name='sandwiches[]' value='" . $sandwich_item['Name'] . "' /></td>\n";
    print "<td align='right'><input name='quantities[]' size='4' value='" . $quantities[$id] . "' /></td>\n";
    print "</tr>\n";

    print "<tr>\n";
    print "<td>Price:</td>\n";
    print "<td>" . $sandwich_item['Price'] . "</td>\n";
    print "<td><input type='hidden' name='prices[]' value='" . $sandwich_item['Price'] . "' /></td>\n"; 
    print "</tr>\n";
		
    print "<tr>\n";
    print "<td>Description:</td>\n";
    print "<td>" . $sandwich_item['Descr'] . "</td>\n";
    print "<td></td>\n";
    print "</tr>\n";
		
    print "<tr>\n";
    print "<td>Calories:</td>\n";
    print "<td>" . $sandwich_item['Calories'] . "</td>\n";
    print "<td></td>\n";
    print "</tr>\n";
		
    print "<tr>\n";
    print "<td colspan='3'><hr></td>\n";
    print "</tr>\n";
	}
?>
  <tr>
  <td>Address</td>
  <td><textarea name="address" rows="4" cols="50"><?php echo set_value('address'); ?></textarea></td>
  <td></td>
  </tr>

  <tr>
  <td>Pay type</td>
  <td>
  <input type='radio' name='pay_type' value='cash' <?php echo set_radio('pay_type', 'cash'); ?>/>cash
  <input type='radio' name='pay_type' value='credit' <?php echo set_radio('pay_type', 'credit'); ?>/>credit
  </td>
  <td></td>
  </tr>

  <tr>
  <td>Card number</td>
  <td><input type="text" name="card_num" size="20" value="<?php echo set_value('card_num'); ?>" /></td>
  <td></td>
  </tr>

  <tr>
  <td>Expire mm/yyyy</td>
  <td>
      <input type="text" name="month" size="2" value="<?php echo set_value('month'); ?>" />
      /
      <input type="text" name="year" size="4" value="<?php echo set_value('year'); ?>" />
  </td>
  <td></td>
  </tr>

  <tr>
  <td>CVV</td>
  <td><input type="text" name="cvv" size="3" value="<?php echo set_value('cvv'); ?>" /></td>
  <td></td>
  </tr>

  <tr>
  <td>Subscribe newsletter</td>
  <td><input type="checkbox" name="newsletter" value='1' <?php echo set_checkbox('newsletter', '1'); ?> /></td>
  <td></td>
  </tr>

  <tr>
  <td colspan='3'><hr></td>
  </tr>

  <tr>
  <td></td>
  <td></td>
  <td align='right'><input type='submit' name='submit' value='order sandwich'/></td>
  </tr>
  </table>
  </form>
  </body>
</html>
