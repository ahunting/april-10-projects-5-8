<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
      dir="ltr">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <title>Sandwich Shop</title>
  </head>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <body>
    <div style="font-weight: bold; margin-bottom: 20px">Welcome To Whole Foods Sandwich Shop
    </div>
    <div style="clear:both">
      &nbsp;
    </div>

<?php
        print "<p style=\"color:blue;\">Your order:</p>";
        print "<table>";
        print "<tr><td width='200'>sandwich</td> <td width='100'>price</td> <td width='150'>quantity</td></tr>";
        foreach ($quantities as $id => $quantity) {
          if (!empty($quantity)) {
            $sandwich=$sandwiches[$id];
            $price   =$prices[$id];
            print "<tr><td>$sandwich</td> <td>$price</td> <td>$quantity</td></tr>";
          }
        }
        print "<tr><td colspan='3'><hr></td></td>";
        print "<tr><td colspan='2'><p style='color:blue;'>Total pay:$" . number_format($total,2) . "</p></td>";
        print "<td align='right'><a href='orderpage'>goto orderpage</a></td></tr>";
        print "</table>";
    
?>

  </body>
</html>
