<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
      dir="ltr">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <title>Sandwich Shop</title>
  </head>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <body>
    <div style="font-weight: bold; margin-bottom: 20px">Welcome To Whole Foods Sandwich Shop
    </div>
    <div style="clear:both">
      &nbsp;
    </div>

<?php
  /*
  create table orders (
  id int not null auto_increment,
  total decimal(10,2),
  address varchar(100),
  pay_type char(5),
  card_num int(20),
  month int(2),
  year int(4),
  cvv int(3),
  newsletter boolean,
  primary key(id)
  );
  */

  function fill_val($name, $id=0) {
    if ($_SERVER['REQUEST_METHOD']=='POST') {
      if (empty($_POST[$name])) {
        $val="";
      }elseif ($id<0) {
        if (isset($_POST[$name])) {
          $val='1';
        }else{
          $val='0';
        }
      }elseif ($id>0) {
        $var=$_POST[$name];
        $val=$var[$id];
      }else{
        $val=$_POST[$name];
      }
    }else{
      $val="";
    }
    return htmlspecialchars($val);
  }

  if ($_SERVER['REQUEST_METHOD']=='POST') {
    // connect to database
    $con = mysql_connect('2011.ispace.ci.fsu.edu', 'amh11u', 'cvrd2n8w');
    mysql_select_db('amh11u_sandwiches', $con);

    // retrieve the form
    $sandwiches  = $_POST['sandwiches'];
    $quantities  = $_POST['quantities'];
    $prices      = $_POST['prices'];
    $address     = mysql_real_escape_string($_POST['address'], $con);
    if (isset($_POST['pay_type'])) $pay_type = $_POST['pay_type'];
    $card_num    = mysql_real_escape_string($_POST['card_num'], $con);
    $month       = mysql_real_escape_string($_POST['month'], $con);
    $year        = mysql_real_escape_string($_POST['year'], $con);
    $cvv         = mysql_real_escape_string($_POST['cvv'], $con);
    $newsletter  = 0;
    if (isset($_POST['newsletter'])) $newsletter=1;

    // check the input is valid
    $error=0;

    // check quantity
    $total=0;
    foreach ($quantities as $quantity) {
      if (!empty($quantity)) $total += $quantity;
    }
    if ($total==0) {
      print "<p style=\"color:red;\">You must input the quantity of the sandwiches</p>";
      $error++;
    }

    // check address
    if (empty($address)) {
      print "<p style=\"color:red;\">You must input the address</p>";
      $error++;
    }

    // check pay type
    if (empty($pay_type)) {
      print "<p style=\"color:red;\">You must input the pay type</p>";
      $error++;
    }else{
      // card number, expire date and cvv must be inputed when pay by credit card
      if ($pay_type=='credit') {
        if (empty($card_num)) {
          print "<p style=\"color:red;\">You must input card number when pay by credit card</p>";
          $error++;
        }
        if (empty($month)) {
          print "<p style=\"color:red;\">You must input expire month when pay by credit card</p>";
          $error++;
        }
        if (empty($year)) {
          print "<p style=\"color:red;\">You must input expire year when pay by credit card</p>";
          $error++;
        }
        if (empty($cvv)) {
          print "<p style=\"color:red;\">You must input cvv when pay by credit card</p>";
          $error++;
        }
      }
    }

    if ($error==0) {
      // calculate the total pay
      $total=0.0;
      foreach ($quantities as $id => $quantity) {
        if (!empty($quantity)) $total = $total + $prices[$id]*$quantity;
      }
      // add 7.5% tax
      $total = $total + $total * .075;

      // add 1.5% processing fee if pay by credit card
      if ($pay_type=='credit') {
        $total *= 1.015;
      }

      // construct the SQL statement
      if ($pay_type=='credit') {
        $sql ="INSERT INTO orders(total,address,pay_type,card_num,month,year,cvv,newsletter) ";
        $sql.="values($total,'$address','$pay_type',$card_num,$month,$year,$cvv,$newsletter)";
      }elseif ($pay_type=='cash') {
        $sql ="INSERT INTO orders(total,address,pay_type,newsletter) ";
        $sql.="values($total,'$address','$pay_type',$newsletter)";
      }

      // execute SQL statement
      mysql_query($sql, $con);

      // Is the SQL statment successfully executed?
      if (mysql_affected_rows($con)==1) {
        print "<p style=\"color:blue;\">Your order:</p>";
        print "<table>";
        print "<tr><td>sandwich</td><td>&nbsp;</td><td>price</td><td>&nbsp;</td><td>quantity</td></tr>";
        foreach ($quantities as $id => $quantity) {
          if (!empty($quantity)) {
            $sandwich=$sandwiches[$id];
            $price   =$prices[$id];
            print "<tr><td>$sandwich</td><td>&nbsp;</td><td>$price</td><td>&nbsp;</td><td>$quantity</td></tr>";
          }
        }
        print "</table>";
        print "<p style=\"color:blue;\">Total :$".number_format($total,2)."</p>";
      }else{
        print '<p class="error">Could not submit your order:<br />'
              .mysql_error($con).'</p>';
        print $sql;
      }
    }

    // close the connection to database
    mysql_close($con);
    
    print "<hr>";
  }
  
?>

    <form action="orderpage.php" method="post">
    <table>
      <tr>
        <td>Name:</td>
        <td>The Sissy<input type="hidden" name="sandwiches[1]" value="The Sissy" /></td>
        <td align="right"><input name="quantities[1]" size="4" value="<?php print fill_val('quantities',1); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$4.99</td>
        <td><input type="hidden" name="prices[1]" value="4.99" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Just veggies; no meat on a whole wheat bun</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">450</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Corleone<input type="hidden" name="sandwiches[2]" value="The Corleone" /></td>
        <td align="right"><input name="quantities[2]" size="4" value="<?php print fill_val('quantities',2); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$5.50</td>
        <td><input type="hidden" name="prices[2]" value="5.50" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Ham and salami with lettuce tomato and onions</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">980</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Mediterranean<input type="hidden" name="sandwiches[3]" value="The Mediterranean" /></td>
        <td align="right"><input name="quantities[3]" size="4" value="<?php print fill_val('quantities',3); ?>" ></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$6.99</td>
        <td><input type="hidden" name="prices[3]" value="6.99" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Hummus, turkey, tomatoes, and olives on whole wehat</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">500</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Greasy Pizza<input type="hidden" name="sandwiches[4]" value="The Greasy Pizza" /></td>
        <td align="right"><input name="quantities[4]" size="4" value="<?php print fill_val('quantities',4); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$8.00</td>
        <td><input type="hidden" name="prices[4]" value="8.00" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Meatballs, sauce, pepperoni, and cheese on a white bun</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">1500</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Plain and Simple<input type="hidden" name="sandwiches[5]" value="The Plain and Simple" /></td>
        <td align="right"><input name="quantities[5]" size="4" value="<?php print fill_val('quantities',5); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$5.50</td>
        <td><input type="hidden" name="prices[5]" value="5.50" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Turkey, lettuce, tomato, and mayo on a wheat bun</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">650</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Porker<input type="hidden" name="sandwiches[6]" value="The Porker" /></td>
        <td align="right"><input name="quantities[6]" size="4" value="<?php print fill_val('quantities',6); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$7.99</td>
        <td><input type="hidden" name="prices[6]" value="7.99" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">Pulled pork, bbq sauce, and cream cheese on a toasted wheat bun</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">1350</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td>The Sub of Insanit<input type="hidden" name="sandwiches[7]" value="The Sub of Insanit" /></td>
        <td align="right"><input name="quantities[7]" size="4" value="<?php print fill_val('quantities',7); ?>"/></td>
      </tr>
      <tr>
        <td>Price:</td>
        <td>$9.99</td>
        <td><input type="hidden" name="prices[7]" value="9.99" /></td>
      </tr>
      <tr>
        <td>Description:</td>
        <td colspan="2">The works; all of our toppings on one sandwich</td>
      </tr>
      <tr>
        <td>Calories:</td>
        <td colspan="2">2200</td>
      </tr>
      <tr>
        <td colspan="3"><hr></td>
      </tr>

  <tr><td>Address</td><td>&nbsp;&nbsp; <textarea name="address" rows="4" cols="50"><?php print fill_val('address'); ?></textarea></td><td></td></tr>
  <tr><td>Pay type</td><td>&nbsp;&nbsp; <?php
    if (fill_val('pay_type')=='cash') {
      print "<input type=\"radio\" name=\"pay_type\" value=\"cash\" checked/>cash";
    }else{
      print "<input type=\"radio\" name=\"pay_type\" value=\"cash\" />cash";
    }
    if (fill_val('pay_type')=='credit') {
      print "<input type=\"radio\" name=\"pay_type\" value=\"credit\" checked/>credit";
    }else{
      print "<input type=\"radio\" name=\"pay_type\" value=\"credit\" />credit";
    }
  ?></td><td></td></tr>
  <tr><td>Card number</td><td>&nbsp;&nbsp; <input type="text" name="card_num" size="20" value="<?php print fill_val('card_num'); ?>" /></td><td></td></tr>
  <tr><td>Expire mm/yyyy</td><td>&nbsp;&nbsp; <input type="text" name="month" size="2" value="<?php print fill_val('month'); ?>" />/<input type="text" name="year" size="4" value="<?php print fill_val('year'); ?>" /></td><td></td></tr>
  <tr><td>CVV</td><td>&nbsp;&nbsp; <input type="text" name="cvv" size="3" value="<?php print fill_val('cvv'); ?>" /></td><td></td></tr>
  <tr><td>Subscribe newsletter</td><td>&nbsp;&nbsp; <?php
    if (fill_val('newsletter',-1)=='1') {
      print "<input type=\"checkbox\" name=\"newsletter\" checked/>";
    }else{
      print "<input type=\"checkbox\" name=\"newsletter\" />";
    }
  ?></td><td></td></tr>
  <tr><td></td><td>&nbsp;&nbsp; <input type="submit" name="submit" value="order sandwich"/></td><td></td></tr>

    </table>
    </form>
  </body>
</html>
