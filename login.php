<?php
    include("config.php");
    include("db.class.php");
    
    //This page is going to use sessions
    session_start();

 

    if (count($_POST) > 0) {
        
        //1. Validate the inputs
        
        //Username must be alphanumeric
        //No longer than 50 character
        //It is required
        
        //Password is required        
        
        //2. Check the username and password against the database
        $username = $_POST['username'];
        $password = $_POST['password'];
 
        $db_obj=new Db($db_host, $db_name, $db_user, $db_pass);
        $found = $db_obj->validate_user($username, $password);
       
        
        //3. If everything checks out, [create a login session]
        if ($found === true) {
            
            //Add info to the session array to indicate that the user is logged-in
            $_SESSION['is_logged_in'] = true;
            $_SESSION['username'] = $username;
            
            $message = "Welcome.  You are logged-in.";
         header('Location: http://localhost/Assignment5/orders.php');   
            
       }
        else {
            $message = "Username and password not matched!";
        }
       
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Login Page</h1>        
        <?php 
        if (isset($message)) {
            echo "<p>" . $message . "</p>";
        }
        ?>
        
        <form method="post" action='login.php'>
            
            <p>
                <label for='Username'>Username</label>
                <input type='text' name='username' id='un' />
            </p>
            
            <p>
                <label for='pw'>Password</label>
                <input type='password' name='password' id='pw' />
            </p>
            
            <p>
                <button type='submit'>Login!</button>
            </p>
            
        </form>
    </body>
</html>