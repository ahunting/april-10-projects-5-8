<?php

class Db
{
    private $host;
    
    private $user;
    
    private $pass;
    
    private $dbname;
    
    private $conn;
    
    public function __construct($host, $dbname, $user, $pass = '')
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;
        
        $this->connect();
    }
    
    public function getData($table)
    {
        $result = $this->conn->query("SELECT * FROM {$table};");
        
        $arr = array();
        
        while($row = $result->fetch_assoc()) {
            $arr[] = $row;
        }
        
        return $arr;        
    }
    
    private function connect()
    {
        $conn = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
        
        if ($conn->connect_error != null) {
            throw new Exception("Could not connect to the database: ". $conn->connect_error);
        }
        
        $this->conn = $conn;
    }
}


?>

